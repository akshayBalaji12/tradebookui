import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import { Login } from './components/Login';
import ProtectedRoute from './components/ProtectedRouter';
import { TradingPage } from './components/TradingPage';
import { Register } from './components/Register';
import { StockGraph } from './components/StockGraph';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#CE93D8'
    },
    type: 'dark'
  }
});

function App() {

  return (

    <MuiThemeProvider theme={theme}>
      <Router>

        <Route path="/" exact component = {Login} />
        <Route path="/register" exact component = {Register} />
        <ProtectedRoute path="/trade" exact component = {TradingPage} />
        <Route path="/stockchart" exact component = {StockGraph} />
        {/* <ProtectedRoute path="/trade" exact component = {() => { 
              window.location.href = '192.168.0.107:4200/trades'; 
              return null;
          }} /> */}

      </Router>
    </MuiThemeProvider>

  );
}

export default App;