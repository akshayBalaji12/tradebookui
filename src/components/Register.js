import React from 'react';
import '../css/Register.css';
import {TextField} from '@material-ui/core';
import { Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const CustomButton = withStyles({
    root: {
      boxShadow: 'none',
      textTransform: 'none',
      borderRadius:'2em',
      color:'#FFFFFF',
      fontSize: 16,
      margin: '20px',
      padding: '6px 12px',
      border: '1px solid',
      lineHeight: 1.5,
      backgroundColor: '#322c3a',
      borderColor: '#CE93D8',
      fontFamily: 'Product-Sans-Regular',
      '&:hover': {
        backgroundColor: '#CE93D8',
        borderColor: '#CE93D8',
        boxShadow: 'none',
      },
      '&:active': {
        boxShadow: 'none',
        backgroundColor: '#0062cc',
        borderColor: '#005cbf',
      },
      '&:focus': {
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      },
    },
  })(Button);

const CustomTextField = withStyles({
    root: {
      '& .MuiFormLabel-root': {
        color: '#CE93D8'
      },
      '& label.Mui-focused': {
        color: '#CE93D8',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#CE93D8',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: '#CE93D8',
          color: '#FAFAFA'
        },
        '&:hover fieldset': {
          borderColor: '#CE93D8',
        },
        '&.Mui-focused fieldset': {
          borderColor: '#CE93D8',
        },
      }     
    }
  })(TextField);

export class Register extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            name: '',
            confPassword: '',
            isRegistered: false,
            isError: false,
            message: ''
        };
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.handleOnClick = this.handleOnClick.bind(this);
    }

    componentDidMount() {
      document.title = "Register";
    }

    handleOnChange = (e) => {

        this.setState({
            [e.target.name]: e.target.value
        })

    }

    handleOnClick = () => {

        if(this.state.confPassword === this.state.password) {

          const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 
              name: this.state.name,
              email: this.state.email,
              password: this.state.password
             })
          };
    
          fetch('http://citichennailinux7.conygre.com:8080/register', requestOptions)
            .then(response => response.json())
            .then(data => {
    
              if(data.responseStatus === 200) {
                this.setState({
                  isRegistered: true,
                  message: data.message
                });
              } else {
                this.setState({
                  isError: true,
                  message: data.message
                });
              }          
    
            });

        }
      
    }

    handleOnSubmit() {

        this.props.history.push("/");

    }

    render() {

        let redirectUser = window.localStorage.getItem("auth");
        let isMessage = this.state.isError || this.state.isRegistered;

        if(this.state.isRegistered) {
          setTimeout(this.handleOnSubmit, 1000);
        }

        if(redirectUser) {
            this.props.history.push("/trade");
        }   

        const MessageField = () => {

          return (
          <h6 class="message">{this.state.message}</h6>
          )
  
        }

        return (

            <div className="App">
                <header className="App-header">
                    <h1 id="Title">Stock Trading App</h1>
                </header>
                <div className="loginContainer">
                <div className="formContainer">
                <div className="header">
                    <h3>Create an account</h3>
                </div>
                <form className="registerForm">
                    <CustomTextField autoComplete="off" onChange={this.handleOnChange} id="outlined-basic" type="text" style={{margin: '5px', width: '300px'}}label="Name" variant="outlined" name="name"/>
                    <CustomTextField autoComplete="off" onChange={this.handleOnChange} id="outlined-basic" type="text" style={{margin: '5px', width: '300px'}} label="Email Address" variant="outlined" name="email"/>
                    <CustomTextField onChange={this.handleOnChange} id="outlined-basic" type="password" style={{margin: '5px', width: '300px'}} label="Password" variant="outlined" name="password"/>
                    <CustomTextField onChange={this.handleOnChange} id="outlined-basic" type="password" style={{margin: '5px', width: '300px'}} label="Confirm Password" variant="outlined" name="confPassword"/>
                    <CustomButton variant="contained" color="primary" disableRipple onClick={this.handleOnClick}>Register</CustomButton>
                    {isMessage ? <MessageField /> : null}
                </form>
                <div>
                    <h4 class="text">Already have an account? <a href="javascrip:;" class="text" id="signInLink" onClick={this.handleOnSubmit}>Sign In</a></h4>
                </div>
                </div>
            </div>
            {/*empty div to create space*/}
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            </div>
          
        );

    }

}