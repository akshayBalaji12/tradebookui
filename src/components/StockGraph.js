import React from 'react';
import CanvasJSReact from './canvasjs.stock.react';
import '../css/StockGraph.css';
import { Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import {TextField} from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import { AddTradeComponent } from './AddTradeComponent';

const CustomButton = withStyles({
    root: {
      boxShadow: 'none',
      textTransform: 'none',
      color:'#FFFFFF',
      fontSize: 16,
      margin: '20px',
      padding: '6px 12px',
      border: '1px solid',
      lineHeight: 1.5,
      backgroundColor: '#322c3a',
      borderColor: '#CE93D8',
      fontFamily: 'Product-Sans-Regular',
      '&:hover': {
        backgroundColor: '#CE93D8',
        borderColor: '#CE93D8',
        boxShadow: 'none',
      },
      '&:active': {
        boxShadow: 'none',
        backgroundColor: '#0062cc',
        borderColor: '#005cbf',
      },
      '&:focus': {
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      },
    },
  })(Button);

const CustomTextField = withStyles({
  root: {
      '& .MuiFormLabel-root': {
      color: '#CE93D8'
      },
      '& label.Mui-focused': {
      color: '#CE93D8',
      },
      '& .MuiInput-underline:after': {
      borderBottomColor: '#CE93D8',
      },
      '& .MuiOutlinedInput-root': {
      '& fieldset': {
          borderColor: '#CE93D8',
          color: '#FAFAFA'
      },
      '&:hover fieldset': {
          borderColor: '#CE93D8',
      },
      '&.Mui-focused fieldset': {
          borderColor: '#CE93D8',
      },
      }     
  }
})(TextField);

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSStockChart = CanvasJSReact.CanvasJSStockChart;

const style = {margin: '5px', width: '300px'};

const tradeType = ["SELL", "BUY"];

export class StockGraph extends React.Component {

   constructor(props) {
       super(props);
       this.state = { isSelected: false, stock: '', dataPoints:[], dataPoints1: [], dataPoints2: [], dataPoints3: [] };
       this.handleOnChange = this.handleOnChange.bind(this);
       this.handleOnBack = this.handleOnBack.bind(this);
     }

     handleOnBack = () =>{
      console.log(window.localStorage.getItem("userID"))
      this.props.history.push({
        pathname: "/trade",
        state: {userID: window.localStorage.getItem("userID")}
      });
    }

     handleOnChange = (option) => {

        let stock = option.split(" - ")[0];
        this.setState({
          isSelected: true
        });

        fetch("http://api.marketstack.com/v1/eod?access_key=4600a3a0b8453fc34c4cb2184f069ff7&symbols="+stock)
          .then(res => res.json())
          .then(data => {
            var dataPoints = [];
            for(var i = 0; i < data['data'].length; i++){
              dataPoints.push({x: new Date(data['data'][i].date), y: Number(data['data'][i].close)});
            }

            this.setState({
              dataPoints: dataPoints,
              stock: stock
            });

          });

       fetch("http://api.marketstack.com/v1/eod?access_key=4600a3a0b8453fc34c4cb2184f069ff7&symbols="+stock)
         .then(res => res.json())
         .then(
           (data) => {
             var dps1 = [], dps2 = [], dps3 = [];
             for (var i = 0; i < data['data'].length; i++) {
               dps1.push({
                 x: new Date(data['data'][i].date),
                 y: [
                   Number(data['data'][i].open),
                   Number(data['data'][i].high),
                   Number(data['data'][i].low),
                   Number(data['data'][i].close)
                 ]
               });
               dps2.push({x: new Date(data['data'][i].date), y: Number(data['data'][i].volume)});
               dps3.push({x: new Date(data['data'][i].date), y: Number(data['data'][i].close)});
             }
             this.setState({
               dataPoints1: dps1,
               dataPoints2: dps2,
               dataPoints3: dps3
             });
           }
         );
     }

     render() {

        const stockChart = {
          backgroundColor: "#322c3a",
          theme: "dark1", //"light1", "dark1", "dark2"
          title:{
            text: "Closing Price"
          },
          subtitles: [{
            text: "Symbol : " + this.state.stock
          }],
          charts: [{
            axisX: {
              crosshair: {
                enabled: true,
                snapToDataPoint: true
              }
            },
            axisY: {
              prefix: "$",
              crosshair: {
                enabled: true,
                snapToDataPoint: true,
                valueFormatString: "$#,###.##"
              }
            },
            toolTip: {
              shared: true
            },
            data: [{
              type: "spline",
              name: "Price",
              yValueFormatString: "$#,###.##",
              dataPoints : this.state.dataPoints
            }]
          }],
          navigator: {
            slider: {
              minimum: new Date("2018-08-01"),
              maximum: new Date("2020-12-01")
            }
          }
        };

       const options = {
         theme: "dark1",
         backgroundColor: "#322c3a",
         title:{
           text:"EOD Stock Price"
         },
         subtitles: [{
           text: "Symbol : " + this.state.stock
         }],
         charts: [{
           axisX: {
             lineThickness: 5,
             tickLength: 0,
             labelFormatter: function(e) {
               return "";
             },
             crosshair: {
               enabled: true,
               snapToDataPoint: true,
               labelFormatter: function(e) {
                 return "";
               }
             }
           },
           axisY: {
             title: "Ticker Price",
             prefix: "$",
             tickLength: 0
           },
           toolTip: {
             shared: true
           },
           data: [{
             name: "Price (in USD)",
             yValueFormatString: "$#,###.##",
             type: "candlestick",
             dataPoints : this.state.dataPoints1
           }]
         },{
           height: 100,
           axisX: {
             crosshair: {
               enabled: true,
               snapToDataPoint: true
             }
           },
           axisY: {
             title: "Volume",
             prefix: "$",
             tickLength: 0
           },
           toolTip: {
             shared: true
           },
           data: [{
             name: "Volume",
             yValueFormatString: "$#,###.##",
             type: "column",
             dataPoints : this.state.dataPoints2
           }]
         }],
         navigator: {
           data: [{
             dataPoints: this.state.dataPoints3
           }],
           slider: {
             minimum: new Date("2018-08-01"),
             maximum: new Date("2020-12-01")
           }
         }
       };

       const containerProps = {
        width: "50%",
        margin: "10px"
      };

      const containerProp = {
        width: "50%",
        margin: "10px"
      };

       const ChartContainer = () => {

        return (
          <div className="pageBody">
            <div className="chartContainer">
            <CanvasJSStockChart className="chart" containerProps={containerProp} options = {options}/>
            <CanvasJSStockChart className="chart" containerProps={containerProps} options = {stockChart}/>
            </div>
            <CustomButton color="primary" id="backButton" onClick={this.handleOnBack}>Back</CustomButton>
          </div>
        )

       }

       return (
         <div className="container">
           <AddTradeComponent onOption={this.handleOnChange} />
           {this.state.isSelected ? <ChartContainer /> : null}
         </div>
       );
     }
   }