import React from 'react';
import '../css/TradingPage.css';
import { withStyles } from '@material-ui/core/styles';
import {TextField} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

const CustomTextField = withStyles({
    root: {
        '& .MuiFormLabel-root': {
        color: '#CE93D8'
        },
        '& label.Mui-focused': {
        color: '#CE93D8',
        },
        '& .MuiInput-underline:after': {
        borderBottomColor: '#CE93D8',
        },
        '& .MuiOutlinedInput-root': {
        '& fieldset': {
            borderColor: '#CE93D8',
            color: '#FAFAFA'
        },
        '&:hover fieldset': {
            borderColor: '#CE93D8',
        },
        '&.Mui-focused fieldset': {
            borderColor: '#CE93D8',
        },
        }     
    }
})(TextField);

let sym = [];

export class AddTradeComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            trade: {
                ticker: '',
                price: 0.0,
                quantity: 0,
                type: ''
            },
            symbols: [],
            company: ''
        }

        this.changeName = this.changeName.bind(this);
        this.getValue = this.getValue.bind(this);

    }

    componentWillUpdate(nextProps, nextState) {
        if (
          this.state.symbols !==
          nextState.symbols
        ) {
          sym = (nextState.symbols);
        }
      }

    getSymbols = (query) => {

        const proxyUrl = `https://cors-anywhere.herokuapp.com/`;
        const url = "http://d.yimg.com/autoc.finance.yahoo.com/autoc?query="+query+"&region=1&lang=en";
        const requestOptions = {
            method: 'GET',
            headers: {
                'Access-Control-Allow-Origin' : '*'
            }
        };

        fetch(proxyUrl + url, requestOptions)
            .then(result => result.json())
            .then(data => {
                this.setState({
                    symbols: data.ResultSet.Result
                });
            }); 

    }

    getValue(e) {
        const value = e.target.textContent;
        this.props.onOption(value);
    }

    changeName = (e, v) => {

        this.setState({
            company: v
        }, this.getSymbols(v));

    }

    render() {

        const style = {margin: '5px', width: '300px'};
        
        return(
            <Autocomplete
                onInputChange={this.changeName}
                id="combo-box-demo"
                options={sym}
                getOptionLabel={(option) => option.name}
                renderOption={(option) => (
                    <React.Fragment>
                        {option.symbol} - {option.name}
                    </React.Fragment>
                    )}
                onChange={this.getValue}
                style={style}
                renderInput={(params) => <CustomTextField {...params} label="Enter Stock" variant="outlined" />}
                />
        )

    }

}