import React from 'react';
import "../css/TradeContainer.css";

const TradeCard = (props) => {

    let style = { backgroundColor: "#1f1b24" };

    const handleOnClick = () => {
        props.onChange(props.tradeDetails);
    }

    return (

        <div className="card" style={style} onClick={handleOnClick}>
            <div className="center">
                <p className="text">Stock: <span className="value">{props.tradeDetails.ticker}</span></p>
                <p className="text">Stock Price: <span className="value">{props.tradeDetails.price} &nbsp; USD</span></p>
                <p className="text">Quantity: <span className="value">{props.tradeDetails.quantity}</span></p>
                <p className="text">Status: <span className="value">{props.tradeDetails.state}</span></p>
                <p className="text">Stock Type: <span className="value">{props.tradeDetails.type}</span></p>
            </div>
        </div>

    )

}

export const TradeContainer = (props) => {

    const trades = props.trades;

    return (

        <div className="cardContainer">
            {trades.map((trade) => (
                <TradeCard key={trade._id} onChange={props.onChange} tradeDetails={trade} />
            ))}
        </div>

    )

}