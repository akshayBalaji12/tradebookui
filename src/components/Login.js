import React from 'react';
import '../css/Login.css';
import {TextField} from '@material-ui/core';
import { Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { BrowserRouter as Route} from 'react-router-dom'
import * as AuthService from "../service/AuthService";
import { Register } from './Register';

const CustomButton = withStyles({
    root: {
      boxShadow: 'none',
      textTransform: 'none',
      borderRadius:'2em',
      color:'#FFFFFF',
      fontSize: 16,
      margin: '20px',
      padding: '6px 12px',
      border: '1px solid',
      lineHeight: 1.5,
      backgroundColor: '#322c3a',
      borderColor: '#CE93D8',
      fontFamily: 'Product-Sans-Regular',
      '&:hover': {
        backgroundColor: '#CE93D8',
        borderColor: '#CE93D8',
        boxShadow: 'none',
      },
      '&:active': {
        boxShadow: 'none',
        backgroundColor: '#0062cc',
        borderColor: '#005cbf',
      },
      '&:focus': {
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      },
    },
  })(Button);

const CustomTextField = withStyles({
    root: {
      '& .MuiFormLabel-root': {
        color: '#CE93D8'
      },
      '& label.Mui-focused': {
        color: '#CE93D8',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#CE93D8',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: '#CE93D8',
          color: '#FAFAFA'
        },
        '&:hover fieldset': {
          borderColor: '#CE93D8',
        },
        '&.Mui-focused fieldset': {
          borderColor: '#CE93D8',
        },
      }     
    }
  })(TextField);

export class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userID: '',
            name: '',
            email: '',
            password: '',
            isError: false,
            errorMessage: ''
            
        };
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.handleOnClick = this.handleOnClick.bind(this);
    }

    componentDidMount() {
      document.title = "Login";
    }

    handleOnChange = (e) => {

        this.setState({
            [e.target.name]: e.target.value
        })

        console.log(this.state.email);

    }

    handleOnClick = () => {

      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ 
          email: this.state.email,
          password: this.state.password
         })
      };

      fetch('http://citichennailinux7.conygre.com:8080/login', requestOptions)
        .then(response =>{
          console.log(response);
          return response.json();
        } )
        .then(responseData => {
          console.log(responseData);
          if(responseData.responseStatus === 200) {

            AuthService.login();

            this.setState({
              userID: responseData.data.userId,
              name: responseData.data.name,
              isError: false,
              errorMessage: ''
            });

            window.localStorage.setItem("userID", this.state.userID);
            window.localStorage.setItem("userName", this.state.name);

          } else {
            
            this.setState({
              isError: true,
              errorMessage: responseData.message
            });
          }          

        });
      
    }

    handleOnSubmit() {

        this.props.history.push("/register");

    }

    render() {

      let redirectUser = window.localStorage.getItem("auth");

      const ErrorField = () => {

        return (
        <h6 class="error">{this.state.errorMessage}</h6>
        )

      }

      if(redirectUser) {
        this.props.history.push({
          pathname: '/trade',
          state: { userID: this.state.userID }
        });
      }

      return (

        <div className="App">
          <header className="App-header">
            <h1 id="Title">Stock Trading App</h1>
          </header>
          <div className="loginContainer">
            <div className="formContainer">
              <div className="header">
                  <h3>Welcome</h3>
              </div>
              <form className="loginForm">
                  <CustomTextField autoComplete="off" onChange={this.handleOnChange} value={this.state.email} id="outlined-email" style={{margin: '10px', width: '300px'}} label="Email Address" variant="outlined" name="email"/>
                  <CustomTextField onChange={this.handleOnChange} value={this.state.password}  id="outlined-pass" type="password" style={{margin: '10px', width: '300px'}} label="Password" variant="outlined" name="password"/>
                  <CustomButton variant="contained" color="primary" disableRipple onClick={this.handleOnClick}>Login</CustomButton>
                  {this.state.isError ? <ErrorField /> : null}
              </form>    
              <div>
                <h4 className="text">Create a new account here - <a href="javascript:;" className="text" id="signInLink" onClick={this.handleOnSubmit}>Register</a></h4>
                <Route path="/register" component={Register} />
              </div>
            </div>
          </div>
          {/*empty div to create space*/}
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
          
      );

    }

}