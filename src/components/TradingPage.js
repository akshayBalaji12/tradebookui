import React, { useState } from 'react';
import '../css/TradingPage.css';
import { TradeContainer } from './TradeContainer';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { withStyles } from '@material-ui/core/styles';
import * as AuthService from "../service/AuthService";
import { Button } from '@material-ui/core';
import {TextField} from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { StockGraph } from './StockGraph';
import { BrowserRouter as Route} from 'react-router-dom';
import { AddTradeComponent } from './AddTradeComponent';

const CustomButton = withStyles({
    root: {
      boxShadow: 'none',
      textTransform: 'none',
      color:'#FFFFFF',
      fontSize: 16,
      margin: '20px',
      padding: '6px 12px',
      border: '1px solid',
      lineHeight: 1.5,
      backgroundColor: '#322c3a',
      borderColor: '#CE93D8',
      fontFamily: 'Product-Sans-Regular',
      '&:hover': {
        backgroundColor: '#CE93D8',
        borderColor: '#CE93D8',
        boxShadow: 'none',
      },
      '&:active': {
        boxShadow: 'none',
        backgroundColor: '#0062cc',
        borderColor: '#005cbf',
      },
      '&:focus': {
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
      },
    },
  })(Button);

  const TradeButton = withStyles({
    root: {
      boxShadow: 'none',
      textTransform: 'none',
      color:'#FFFFFF',
      fontSize: 16,
      margin: '10px',
      padding: '6px 12px',
      border: '1px solid',
      lineHeight: 1.5,
      backgroundColor: '#CE93D8',
      borderColor: '#CE93D8',
      fontFamily: 'Product-Sans-Regular'
    },
  })(Button);

const CustomTextField = withStyles({
    root: {
        '& .MuiFormLabel-root': {
        color: '#CE93D8'
        },
        '& label.Mui-focused': {
        color: '#CE93D8',
        },
        '& .MuiInput-underline:after': {
        borderBottomColor: '#CE93D8',
        },
        '& .MuiOutlinedInput-root': {
        '& fieldset': {
            borderColor: '#CE93D8',
            color: '#FAFAFA'
        },
        '&:hover fieldset': {
            borderColor: '#CE93D8',
        },
        '&.Mui-focused fieldset': {
            borderColor: '#CE93D8',
        },
        }     
    }
})(TextField);

const WaitingText = () => {
    return (
        <h5 style={{margin: "0%", marginTop: "5px"}}>Please wait..</h5>
    )
}

const GettingText = () => {
    return (
        <h5 style={{margin: "10px"}}>Fetching Price</h5>
    )
}

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

export class TradingPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            trades: [],
            isVisible: false,
            isOpen: false,
            trade: {},
            isError: false,
            errorMessage: null,
            yahooData: []
        };

        this.handleOnLogout = this.handleOnLogout.bind(this);
        this.handleOnToggle = this.handleOnToggle.bind(this);
        this.handleOnEdit = this.handleOnEdit.bind(this);
        this.handleOnClose = this.handleOnClose.bind(this);
        this.handleStockHistory = this.handleStockHistory.bind(this);

    }

    componentDidMount() {

        document.title = "Stock Trader";
        let userID = this.props.location.state.userID;

        if(userID === ""){
            userID = window.localStorage.getItem("userID");
        }

        fetch('http://citichennailinux7.conygre.com:8080/api/v1/'+userID+'/trades')
        .then(response => response.json())
        .then(data => this.setState({
            trades: data
        }));

    }

    handleOnToggle() {

        this.setState({
            isVisible: !this.state.isVisible
        });

    }

    handleOnEdit = (trade) => {

        this.setState({
            isOpen: true,
            trade: trade
        });

        console.log("In edit: " + trade);

    }

    handleOnLogout = () => {

        AuthService.logout();
        if (window.localStorage.getItem("userName") != null) {
            window.localStorage.removeItem("userName");
        }

        window.localStorage.removeItem("userID")

        this.props.history.push('/');

    }

    handleStockHistory = () => {
        this.props.history.push("/stockchart");
    }

    handleOnClose = () => {

        this.setState({
            trade: {},
            isOpen: false
        });

    }

    render() {

        let userID = window.localStorage.getItem("userID");
        let userName = window.localStorage.getItem("userName");
        console.log(this.state.trades);

        const style = {margin: '5px', width: '300px'};

        const tradeType = ["SELL", "BUY"];

        const SimpleDialog = (props) => {

            const {open, onClose} = props;

            return (

                <Dialog maxWidth="xl" open={open} onClose={onClose} TransitionComponent={Transition} keepMounted>
                    <EditTrade />
                </Dialog>

            )

        }

        const EditTrade = () => {

            const [trade, setTrade] = useState({
                id: this.state.trade.id,
                ticker: this.state.trade.ticker,
                price: this.state.trade.price,
                quantity: this.state.trade.quantity,
                type: this.state.trade.type,
                created: this.state.trade.created,
                status: this.state.trade.state
            });

            const[isLoading, setLoading] = useState(false);

            const changeValue = (e) => {
                console.log('select');
                const {value, name} = e.target;

                if(name === "quantity") {
                    if(value >= 0) {
                        setTrade(prev => ({...prev, [name]: parseInt(value)}));
                    }
                } else if(name === "price") {
                    setTrade(prev => ({...prev, [name]: parseFloat(value)}));
                } else {
                    setTrade(prev => ({...prev, [name]: value}));
                }
                
            }

            const deleteTrade = () => {

                setLoading(true);

                const requestOptions = {
                    method: 'DELETE'
                  };
            
                  fetch('http://citichennailinux7.conygre.com:8080/api/v1/'+userID+'/trades/'+trade.id, requestOptions)
                    .then(response => {

                        if(response.status === 200) {

                            fetch('http://citichennailinux7.conygre.com:8080/api/v1/'+userID+'/trades')
                                .then(response => response.json())
                                .then(data => this.setState({
                                    trades: data,
                                    isVisible: false,
                                    trade: {},
                                    isOpen: false
                                }));
                            toast.success("Trade deleted",{
                                autoClose: 2500,
                                pauseOnHover: false
                            });

                        }

                    })

            }

            const submit = () => {

                setLoading(true);

                const requestOptions = {
                    method: 'PUT',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ 
                        id: trade.id,
                        ticker: trade.ticker,
                        price: trade.price,
                        quantity: trade.quantity,
                        type: trade.type,
                        state: trade.status,
                        created: trade.created
                     })
                  };
            
                  fetch('http://citichennailinux7.conygre.com:8080/api/v1/trades/'+trade.id, requestOptions)
                    .then(response => (response.json())
                    .then(responseData => {
                        if(responseData.responseStatus === 200) {

                            fetch('http://citichennailinux7.conygre.com:8080/api/v1/'+userID+'/trades')
                                .then(response => response.json())
                                .then(data => this.setState({
                                    trades: data,
                                    isVisible: false,
                                    trade: {},
                                    isOpen: false
                                }));
                            toast.success("Updated",{
                                    pauseOnHover: false
                            });  

                        } else {
                            console.log(response.statusText)
                            this.setState({
                                trade: {},
                                isOpen: false,
                                isVisible: false
                            });
                            toast.error(responseData.message,{
                                pauseOnHover: false
                            });
                          }
                        }  
                    ));
                    
        
            }


            return(
                <div className="addContainer">
                    <h5 className="addHeading">Edit Trade</h5>
                    <CustomTextField onChange={e => changeValue(e)} autoComplete="off" id="outlined-basic" type="text" style={style} label="Stock Name" variant="outlined" name="ticker" value={trade.ticker}/>

                    <CustomTextField onChange={e => changeValue(e)} autoComplete="off" id="outlined-basic" type="number" style={style} label="Price" variant="outlined" name="price" value={trade.price}/>

                    <CustomTextField onChange={e => changeValue(e)} autoComplete="off" id="outlined-basic" type="number" min="0" step="1" style={style} label="Quantity" variant="outlined" name="quantity" value={trade.quantity}/>

                    <CustomTextField select onChange={e => changeValue(e)} autoComplete="off" id="outlined-basic" type="text" style={style} label="Trade Type" variant="outlined" name="type" value={trade.type}>
                        {tradeType.map((option) => (
                            <MenuItem key={option} value={option}>
                            {option}
                            </MenuItem>
                        ))}
                    </CustomTextField>
                    {isLoading ? <WaitingText /> : null}
                    <TradeButton onClick={submit} color="primary" style={{color: "white", fontFamily: "Product-Sans-Regular", textTransform: "none"}}>Edit</TradeButton>
                    <h5>Or</h5>
                    <TradeButton onClick={deleteTrade} color="primary" style={{color: "white", fontFamily: "Product-Sans-Regular", textTransform: "none"}}>Delete</TradeButton>
                </div>
            )

        }

        const AddTrade = () => {

            const [trade, setTrade] = useState({
                ticker: '',
                price: 0.0,
                quantity: 0,
                type: ''
            });

            const[isLoading, setLoading] = useState(false);
            const[isGetting, setGetting] = useState(false);

            const changeValue = (e) => {
                const {value, name} = e.target;

                if(name === "quantity") {
                    if(value >= 0) {
                        setTrade(prev => ({...prev, [name]: parseInt(value)}));
                    }
                } else {
                    setTrade(prev => ({...prev, [name]: value}));
                }
                
            }

            const getTicker = (option) => {

                setGetting(true);
                let ticker = option.split(" - ");

                const url = `https://rapidapi.p.rapidapi.com/market/v2/get-quotes?symbols=${encodeURIComponent(ticker[0])}&region=US`;
                const reqOptions = {
                    method: 'GET',
                    headers: {
                        "x-rapidapi-host": "apidojo-yahoo-finance-v1.p.rapidapi.com",
                        "x-rapidapi-key": "6d04c96ecfmsh39f073b2bb7ffb0p1a82ffjsn94e8c1b8ee96",
                        "useQueryString": true
                    }
                }

                fetch(url, reqOptions)
                    .then(response => response.json())
                    .then(data => {
                        setGetting(false);
                        let price = data.quoteResponse.result[0].regularMarketPrice;
                        setTrade(prev => ({...prev, ticker: ticker[0]}));
                        setTrade(prev => ({...prev, price: parseFloat(price)}));
                        console.log(parseFloat(price));
                    });

            }

            const submit = () => {

                setLoading(true);

                const requestOptions = {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ 
                        ticker: trade.ticker,
                        price: trade.price,
                        quantity: trade.quantity,
                        type: trade.type
                     })
                  };
            
                  fetch('http://citichennailinux7.conygre.com:8080/api/v1/'+userID+'/trades', requestOptions)
                    .then(response => {

                        if(response.status === 200) {

                            fetch('http://citichennailinux7.conygre.com:8080/api/v1/'+userID+'/trades')
                                .then(response => response.json())
                                .then(data => this.setState({
                                    trades: data,
                                    isVisible: false
                                }));

                        } else {

                            console.log(trade);

                        }

                    })
        
            }

            return(
                <div className="addContainer">
                    <h5 className="addHeading">Add Trade</h5>
                    <AddTradeComponent style={style} onOption={getTicker} />
                    {isGetting ? <GettingText /> : null}
                    <CustomTextField autoComplete="off" id="outlined-basic" type="number" style={style} label="Price" variant="outlined" name="price" value={trade.price}/>

                    <CustomTextField onChange={e => changeValue(e)} autoComplete="off" id="outlined-basic" type="number" min="0" step="1" style={style} label="Quantity" variant="outlined" name="quantity" value={trade.quantity}/>

                    <CustomTextField select onChange={e => changeValue(e)} autoComplete="off" id="outlined-basic" type="text" style={style} label="Trade Type" variant="outlined" name="type" value={trade.type}>
                        {tradeType.map((option) => (
                            <MenuItem key={option} value={option}>
                            {option}
                            </MenuItem>
                        ))}
                    </CustomTextField>
                    {isLoading ? null : <TradeButton onClick={submit} color="primary" style={{color: "white", fontFamily: "Product-Sans-Regular", textTransform: "none"}}>Add</TradeButton>}
                    {isLoading ? <WaitingText /> : null}
                    <IconButton onClick={this.handleOnToggle}>
                        <CloseIcon/>
                    </IconButton>
                </div>
            )

        }

        const AddButton = () => {

            return(
                <Fab color="primary" aria-label="add" size="small" onClick={this.handleOnToggle}>
                    <AddIcon />
                </Fab> 
            )

        }

        return (
            
            <div className="App">
                <header className="bar header">
                    <div className="headerContainer">
                        <h4 className="headerItem" id="name">Hi, {userName} !</h4>
                        <h2 className="headerItem" id="headerTitle">Stock Trading App</h2>
                        <div className="headerItem" id="headerButton">
                            <CustomButton color="primary" id="headerStock" onClick={this.handleStockHistory}>Stock History</CustomButton>
                            <Route path="/stockchart" component={StockGraph} />
                            <CustomButton color="primary" id="headerLogout" onClick={this.handleOnLogout}>Logout</CustomButton>
                        </div>
                    </div>
                </header>
                <h3 id="Title">Hi, {userName} !</h3>
                <ToastContainer className="error" />
                <div className="container">
                    <h3 id="tradeTitle" style={{backgroundColor:"#322c3a"}}>Your Trades </h3>
                    {this.state.isVisible ? <AddTrade /> : null}
                    {this.state.isVisible ? null : <AddButton />}
                    <SimpleDialog open={this.state.isOpen} onClose={this.handleOnClose}/>
                    <div>
                        <TradeContainer trades={this.state.trades} onChange={this.handleOnEdit}/>
                    </div>
                </div>
            </div>
            
        );

        

    }

}